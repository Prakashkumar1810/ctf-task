package admin

//LoginCreds : stores login Credentials from request
type LoginCreds struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

//SignupCreds : stores signup Credentials from request
type SignupCreds struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

// ResponseData : struct for sending response message and JWT
type ResponseData struct {
	Token   string `json:"token,omitempty"`
	Message string `json:"message"`
}
