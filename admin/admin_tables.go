package admin

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

//Admin : struct for Users table in database
type Admin struct {
	Username string `gorm:"primary_key;type:varchar(30)"`
	Email    string `gorm:"unique;type:varchar(30)"`
	Password string
}

var db *gorm.DB
var err error

//InitTables : func to create tables in DB if not present
func InitTables(dbip, dbString string) {
	db, err = gorm.Open("mysql", dbString)

	if err != nil {
		fmt.Println(err)
		return
	}

	if err := db.AutoMigrate(&Admin{}).Error; err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Admin table initiation Successful")
}

//CloseDB to close db after completion
func CloseDB() {
	db.Close()
	fmt.Println("Admin DB connection closed")
}
