package admin

import (
	"ctf-task/custjwt"
	"encoding/json"
	"net/http"
	"strings"
)

// login for admin
func login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	var creds LoginCreds
	var admin Admin
	var resp ResponseData

	json.NewDecoder(r.Body).Decode(&creds)

	if db.Where("username = ?", creds.Username).First(&admin).Error != nil {
		resp.Message = "Unauthorized"
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(&resp)
		return
	}

	password, err1 := custjwt.HashPassword(creds.Password)

	if err1 != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal error"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	if password != admin.Password {
		resp.Message = "Incorrect Password"
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(&resp)
		return
	}

	token, err := custjwt.GenerateJWT(w, creds.Username)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal Error"
		return
	}
	resp.Token = token
	resp.Message = "Login Successful"
	json.NewEncoder(w).Encode(&resp)
}

// signup for admin
func signup(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-type", "application/json")

	var creds SignupCreds
	var resp ResponseData
	var admin Admin

	json.NewDecoder(r.Body).Decode(&creds)

	if len(creds.Username) < 1 || len(creds.Password) < 8 || !strings.Contains(creds.Email, "@") {
		w.WriteHeader(http.StatusBadRequest)
		resp.Message = "Invalid Input fields"
		json.NewEncoder(w).Encode(&resp)
		return
	} else if err = db.Where("username = ?", creds.Username).First(&admin).Error; err == nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Message = "Username Already Found"
		json.NewEncoder(w).Encode(&resp)
		return
	} else if err = db.Where("email = ?", creds.Email).First(&admin).Error; err == nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Message = "Email Already Found"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	admin.Email = creds.Email
	admin.Username = creds.Username
	admin.Password, err = custjwt.HashPassword(creds.Password)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal error"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	if db.Table("admins").Create(&admin).Error != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal error"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	token, err := custjwt.GenerateJWT(w, creds.Username)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal Error"
		return
	}
	resp.Token = token
	resp.Message = "Registration Successfull"
	json.NewEncoder(w).Encode(&resp)
}
