package custjwt

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var ctfKey = []byte("This is a secret key")

// HashPassword sha256 hash function
func HashPassword(password string) (string, error) {
	hasher := sha256.New()
	_, err := hasher.Write([]byte(password))

	if err != nil {
		return "", err
	}

	return hex.EncodeToString(hasher.Sum(nil)), nil
}

// GenerateJWT to generate a JSON Web Token
func GenerateJWT(w http.ResponseWriter, username string) (string, error) {

	expirationTime := time.Now().Add(30 * time.Hour)

	claims := &Claims{
		Username: username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString(ctfKey)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return "", err
	}
	return tokenString, nil
}

// VerifyJWT to verify JSON Web Token
func VerifyJWT(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		tokenString := r.Header.Get("Authorization")

		claims := &Claims{}

		token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Wrong signing method")
			}
			return ctfKey, nil
		})

		if err != nil {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode(&TempData{Message: "Invalid Token"})
			return
		}

		var ctx context.Context
		if token.Valid {
			ctx = context.WithValue(r.Context(), ContextKey("username"), claims.Username)
			next.ServeHTTP(w, r.WithContext(ctx))
		}
	}
}

//AccountVerification for 2FA during signup ad Forgot password
func AccountVerification(tokenString string) (string, bool) {
	claims := &Claims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Wrong signing method")
		}
		return ctfKey, nil
	})

	if err != nil {
		return "", false
	}

	if token.Valid {
		return claims.Username, true
	}
	return "", false
}

// Check Temporary Function
func check(w http.ResponseWriter, r *http.Request) {
	email := r.Context().Value(ContextKey("username"))

	if email == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	fmt.Fprintf(w, "IT WORKED %s", email)

}
