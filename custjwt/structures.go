package custjwt

import "github.com/dgrijalva/jwt-go"

//Claims : Data to be embedded in the JWT
type Claims struct {
	Username string
	jwt.StandardClaims
}

//TempData : to read JWT from request and send Error messages
type TempData struct {
	Message string `json:"message"`
}

//ContextKey New type for getting Request context key because it throws a warning
type ContextKey string

func (c ContextKey) String() string {
	return string(c)
}
