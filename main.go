package main

import (
	"ctf-task/admin"
	"ctf-task/bidder"
	"ctf-task/control"
	"ctf-task/custjwt"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	dbip             = "localhost:3306"
	dbname           = "mydb"
	localport        = "8080"
	remotedbname     = "nKpSq3urLf"
	remotedbip       = "remotemysql.com:3306"
	remotedbpassword = "PKGSYhK0qj"
	port             = os.Getenv("PORT")
)

// initializing the routes and starting the server
func initHandlers() {
	router := mux.NewRouter()
	sh := http.StripPrefix("/swaggerui/", http.FileServer(http.Dir("./swagger/")))
	router.PathPrefix("/swaggerui/").Handler(sh)
	router.HandleFunc("/listactivebids", custjwt.VerifyJWT(control.GetActiveBids)).Methods("GET")
	bidder.InitHandlers(router)
	admin.InitHandlers(router)
	http.ListenAndServe(":"+localport, router)
}

func main() {
	//dbString := remotedbname + ":" + remotedbpassword + "@(" + remotedbip + ")/" + remotedbname + "?charset=utf8&parseTime=True&loc=Local"
	dbString := "root:@(" + dbip + ")/" + dbname + "?charset=utf8&parseTime=True&loc=Local"
	admin.InitTables(dbip, dbString)
	bidder.InitTables(dbip, dbString)
	control.InitTables(dbip, dbString)
	control.UpdateActiveBids()
	go control.BidTimer()

	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGTERM, syscall.SIGINT)

	go initHandlers()
	defer admin.CloseDB()
	defer bidder.CloseDB()
	defer control.CloseDB()
	fmt.Println("CONTROL-C To stop server")
	<-done
	fmt.Println("Server stopped")
}
