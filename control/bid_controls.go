package control

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/jinzhu/gorm"
)

//Mutex to prevent multiple writes to activeBids at a time
var mu sync.Mutex

//ActiveBids array of all activebids
var activeBids []ActiveBid
var tzero time.Time

//UpdateActiveBids : read all active bids from db
func UpdateActiveBids() {
	mu.Lock()
	defer mu.Unlock()
	if db.Where("active = ?", true).Find(&activeBids).Error != nil {
		fmt.Println("Error")
		return
	}
	for i, bid := range activeBids {
		fmt.Println(bid)
		if activeBids[i].New == 1 {
			activeBids[i].Modified = time.Now()
			activeBids[i].New = 2
		}
	}
}

//GetActiveBids : returns active bids
func GetActiveBids(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "applicaton/json")

	for i := range activeBids {
		his := &History{}
		if db.Where("product_id = ?", activeBids[i].ProductID).First(his).Error == nil {
			activeBids[i].BidHistory = his
		}
	}

	json.NewEncoder(w).Encode(&activeBids)
}

//updateBidHistory to update the bidding history
func updateBidHistory(productid int, username string, amount float64) {
	var his History

	if err := db.Where("product_id = ?", productid).First(&his).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			his.ProductID = productid
			db.Create(&his)
		}
		fmt.Println(err)
		return
	}

	his.Prevamt4 = his.Prevamt3
	his.Prevamt3 = his.Prevamt2
	his.Prevamt2 = his.Prevamt1
	his.Prevamt1 = amount
	his.Prevusr4 = his.Prevusr3
	his.Prevusr3 = his.Prevusr2
	his.Prevusr2 = his.Prevusr1
	his.Prevusr1 = username
	db.Table("histories").Where("product_id = ?", his.ProductID).Updates(&his)
}

//MakeBid to make a bid. returns message and success state(bool)
func MakeBid(username string, productid int, amount float64) (string, bool) {
	var i int
	var bid ActiveBid
	found := false
	for i, bid = range activeBids {
		if bid.ProductID == productid {
			found = true
			break
		}
	}
	if !found {
		return "Product not found", false
	}
	if amount <= bid.CurrentBid {
		return "Amount must be greater than current price", false
	}

	//Updating bid history
	updateBidHistory(bid.ProductID, bid.Buyer, bid.CurrentBid)

	mu.Lock()
	defer mu.Unlock()
	activeBids[i].CurrentBid = amount
	activeBids[i].Buyer = username
	activeBids[i].Modified = time.Now()
	for db.Save(&activeBids[i]).Error != nil {
	}
	return "Bidding successful", true
}

//AddBid : func to adda bid to the bids table
func AddBid(bid ActiveBid) (string, bool) {
	bid.Modified = tzero.Add(time.Minute)
	if err := db.Create(&bid).Error; err != nil {
		return err.Error(), false
	}

	//Updating active bids array as new bid may be active
	UpdateActiveBids()

	return "Bid added successfully", true
}

//DeleteBid : Function to delete a bid from bidding table
func DeleteBid(productid int) (string, bool) {
	var bid ActiveBid
	if err := db.Where("product_id = ?", productid).First(&bid).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return "Product not found", false
		}
		return "Unknown Error", false
	}

	if db.Delete(&bid).Error != nil {
		return "Unknown Error", false
	}
	var his History

	if err := db.Where("product_id = ?", productid).First(&his).Error; err == nil {
		db.Delete(&his)
	}
	//Updating active bids array as we may have deleted an active bidding
	UpdateActiveBids()
	return "Deletion Successful", true
}

//UpdateBid : update values of a bid
func UpdateBid(bid ActiveBid) (string, bool) {
	if bid.ProductID <= 0 {
		return "product_id must be specified", false
	}

	var tempbid ActiveBid
	if err := db.Where("product_id = ?", bid.ProductID).First(&tempbid).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return "Product not found", false
		}
		return "Unknown Error", false
	}

	if !bid.Active && tempbid.Active {
		tempbid.Active = false
		db.Save(&tempbid)
		bid.New = 1
	}

	db.Model(&tempbid).Updates(bid)

	//Updating active bids array as we may have paused or started an active bidding
	UpdateActiveBids()

	return "Update Successfull", true
}

//BidTimer : checks the activeBids array every sec if value doesn't change for 30 sec. update bid as complete
func BidTimer() {
	var comp []ActiveBid
	for {
		l := len(activeBids)
		mu.Lock()
		for i := 0; i < l; i++ {
			if time.Now().Sub(activeBids[i].Modified) > time.Minute {
				comp = append(comp, activeBids[i])
				activeBids = append(activeBids[:i], activeBids[i+1:]...)
				i--
				l--
			}
		}
		mu.Unlock()
		for _, bid := range comp {
			db.Create(&CompletedBid{
				ProductID:   bid.ProductID,
				ProductName: bid.ProductName,
				InitialBid:  bid.InitialBid,
				FinalBid:    bid.CurrentBid,
				Buyer:       bid.Buyer,
			})
			db.Delete(&bid)
		}
		comp = nil
		time.Sleep(time.Second * 3)
	}
}
