package control

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

//ActiveBid struct for Bids table in database
type ActiveBid struct {
	ProductID   int       `gorm:"primary_key;auto_increment" json:"product_id"`
	ProductName string    `gorm:"type:varchar(30)" json:"product_name"`
	InitialBid  float64   `json:"initial_bid"`
	CurrentBid  float64   `json:"current_bid,omitempty"`
	Buyer       string    `gorm:"type:varchar(30)" json:"buyer,omitempty"`
	Active      bool      `gorm:"default:FALSE" json:"active"`
	Modified    time.Time `json:"-"`
	BidHistory  *History  `gorm:"-" json:"bid_history,omitempty"`
	New         int       `gorm:"default:1" json:"_"`
}

//CompletedBid struct for Bids table in database
type CompletedBid struct {
	ProductID   int      `gorm:"primary_key;auto_increment" json:"product_id"`
	ProductName string   `gorm:"type:varchar(30)" json:"product_name"`
	InitialBid  float64  `json:"initial_bid"`
	FinalBid    float64  `json:"current_bid,omitempty"`
	Buyer       string   `gorm:"type:varchar(30)" json:"buyer,omitempty"`
	BidHistory  *History `gorm:"-" json:"bid_history,omitempty"`
}

//History of bidders and prices
type History struct {
	ProductID int     `gorm:"unique" json:"-"`
	Prevamt1  float64 `json:"previous_val1,omitempty"`
	Prevusr1  string  `gorm:"type:varchar(30)" json:"previous_usr1,omitempty"`
	Prevamt2  float64 `json:"previous_val2,omitempty"`
	Prevusr2  string  `gorm:"type:varchar(30)" json:"previous_usr2,omitempty"`
	Prevamt3  float64 `json:"previous_val3,omitempty"`
	Prevusr3  string  `gorm:"type:varchar(30)" json:"previous_usr3,omitempty"`
	Prevamt4  float64 `json:"previous_val4,omitempty"`
	Prevusr4  string  `gorm:"type:varchar(30)" json:"previous_usr4,omitempty"`
}

var db *gorm.DB
var err error

//InitTables : func to create tables in DB if not present
func InitTables(dbip, dbString string) {
	db, err = gorm.Open("mysql", dbString)

	if err != nil {
		fmt.Println(err)
		return
	}

	if err := db.AutoMigrate(&ActiveBid{}, &CompletedBid{}, &History{}).Error; err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Bids Table initiation Successful")
}

//CloseDB to close db after completion
func CloseDB() {
	db.Close()
	fmt.Println("Control DB connection closed")
}
