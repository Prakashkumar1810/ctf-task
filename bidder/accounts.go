package bidder

import (
	"context"
	"crypto/rand"
	"ctf-task/custjwt"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var unverified []User
var forgot []User

var conf *oauth2.Config

const oauthGoogleURLAPI = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="

//func for calling google signup
func googlemiddleware(w http.ResponseWriter, r *http.Request) {
	var c GoogleCreds
	file, err := ioutil.ReadFile("./googlecreds.json")
	if err != nil {
		fmt.Printf("File error: %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	json.Unmarshal(file, &c)

	conf = &oauth2.Config{
		ClientID:     c.Cid,
		ClientSecret: c.Csecret,
		RedirectURL:  "http://localhost:8080/user/googleverification",
		//RedirectURL: "https://ctf3030.herokuapp.com/user/googleverification",
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
		},
		Endpoint: google.Endpoint,
	}

	// Create oauthState cookie
	oauthState := generateStateOauthCookie(w)
	u := conf.AuthCodeURL(oauthState)
	http.Redirect(w, r, u, http.StatusTemporaryRedirect)

}

// to create oauth state cookie
func generateStateOauthCookie(w http.ResponseWriter) string {
	var expiration = time.Now().Add(365 * 24 * time.Hour)

	b := make([]byte, 16)
	rand.Read(b)
	state := base64.URLEncoding.EncodeToString(b)
	cookie := http.Cookie{Name: "oauthstate", Value: state, Expires: expiration}
	http.SetCookie(w, &cookie)

	return state
}

// callback from google
func oauthGoogleCallback(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	oauthState, _ := r.Cookie("oauthstate")

	var resp ResponseData

	if r.FormValue("state") != oauthState.Value {
		w.WriteHeader(http.StatusUnauthorized)
		resp.Message = "Invalid oAuth state"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	data, err := getUserDataFromGoogle(r.FormValue("code"))
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		resp.Message = err.Error()
		json.NewEncoder(w).Encode(&resp)
		return
	}

	if !data.Verified {
		w.WriteHeader(http.StatusUnauthorized)
		resp.Message = "Your email is not verified"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	var creds SignupCreds
	creds.Email = data.Email
	creds.Username = strings.Split(creds.Email, "@")[0]

	token, err := googleSignupOrLogin(w, creds)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = err.Error()
		json.NewEncoder(w).Encode(&resp)
		return
	}

	resp.Token = token
	resp.Message = "Registration Successfull. Your username is '" + creds.Username + "'"
	json.NewEncoder(w).Encode(&resp)
}

// helper function
func getUserDataFromGoogle(code string) (*GoogleUserInfo, error) {
	// Use code to get token and get user info from Google.

	token, err := conf.Exchange(context.Background(), code)
	if err != nil {
		return nil, fmt.Errorf("code exchange wrong: %s", err.Error())
	}
	response, err := http.Get(oauthGoogleURLAPI + token.AccessToken)
	if err != nil {
		return nil, fmt.Errorf("failed getting user info: %s", err.Error())
	}

	var info GoogleUserInfo

	json.NewDecoder(response.Body).Decode(&info)

	if err != nil {
		return nil, fmt.Errorf("failed read response: %s", err.Error())
	}
	return &info, nil
}

// to login or signup with info got from oAuth
func googleSignupOrLogin(w http.ResponseWriter, creds SignupCreds) (string, error) {

	var user User

	token, err := custjwt.GenerateJWT(w, creds.Username)

	if err != nil {
		return "", err
	}

	if err = db.Where("username = ?", creds.Username).First(&user).Error; err == nil {
		return token, nil
	} else if err = db.Where("email = ?", creds.Email).First(&user).Error; err == nil {
		return "", fmt.Errorf("Email registered with different username")
	}

	user.Email = creds.Email
	user.Username = creds.Username

	if db.Table("users").Create(&user).Error != nil {
		return "", fmt.Errorf("Internal Error")
	}
	return token, nil
}

// gets password from the form and resets it
func resetpassword(w http.ResponseWriter, r *http.Request) {
	username, err := r.Cookie("username")
	newpassword := r.FormValue("password")

	if len(newpassword) < 8 {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Password must be atleast 8 Characters")
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Unknown Error")
		return
	}

	password, ok := custjwt.HashPassword(newpassword)
	if ok != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Unknown Error")
		return
	}

	db.Model(&User{}).Where("username = ?", username.Value).Update("password", password)
	fmt.Fprintf(w, "Reset Successful")
}

//sends verification mail to user
func forgotpassword(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	var creds LoginCreds
	var user User
	var resp ResponseData

	json.NewDecoder(r.Body).Decode(&creds)

	if db.Where("username = ?", creds.Username).First(&user).Error != nil {
		resp.Message = "Username not found"
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(&resp)
		return
	}
	token, err := custjwt.GenerateJWT(w, creds.Username)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal error"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	found := false
	for _, temp := range forgot {
		if temp.Username == user.Username {
			found = true
			break
		}
	}

	if !found {
		forgot = append(forgot, user)
	}

	verifylink := "http://localhost:8080/user/fotgotverification/" + token
	//verifylink := "https://ctf3030.herokuapp.com/user/fotgotverification/" + token

	ok := sendmail(verifylink, user, "Password Reset link:")

	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal error"
		json.NewEncoder(w).Encode(&resp)
		return
	}
	json.NewEncoder(w).Encode(&ResponseData{Message: "Reset password link has been sent to your email"})

}

//verifies and sends a form for new password
func forgotmiddleware(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	var user User
	var i int

	urldata := mux.Vars(r)

	token := urldata["token"]

	username, ok := custjwt.AccountVerification(token)

	if !ok {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, "Cannot verify link try again")
		return
	}

	found := false
	for i, user = range forgot {
		if username == user.Username {
			found = true
			break
		}
	}

	if !found {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, "Cannot verify link try again")
		return
	}

	forgot = append(forgot[:i], forgot[i+1:]...)

	form := "<form action='resetpassword' method='POST'>" +
		"<input type='password' name='password'/>" +
		"<input type='submit' value='change'/>" +
		"</form>"

	cook := &http.Cookie{
		Name:    "username",
		Value:   username,
		Expires: time.Now().Add(time.Minute * 5),
	}
	http.SetCookie(w, cook)

	fmt.Fprintf(w, form)

}

// login for bidder
func login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	var creds LoginCreds
	var user User
	var resp ResponseData

	json.NewDecoder(r.Body).Decode(&creds)

	if db.Where("username = ?", creds.Username).First(&user).Error != nil {
		resp.Message = "Unauthorized/User not found"
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(&resp)
		return
	}

	if len(creds.Password) < 8 {
		w.WriteHeader(http.StatusBadRequest)
		resp.Message = "Incorrect password"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	password, err1 := custjwt.HashPassword(creds.Password)

	if err1 != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal error"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	if password != user.Password {
		resp.Message = "Incorrect Password"
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(&resp)
		return
	}

	token, err := custjwt.GenerateJWT(w, creds.Username)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal Error"
		return
	}
	resp.Token = token
	resp.Message = "Login Successful"
	json.NewEncoder(w).Encode(&resp)
}

// to send verification mail to user
func sendmail(verifylink string, creds User, msg string) bool {
	file, _ := ioutil.ReadFile("sendgridkey.txt")
	sendgridKey := string(file)
	//sendgridKey := os.Getenv("SENDGRID_API_KEY")
	from := mail.NewEmail("Task App", "task@ctf.com")
	subject := "Verification Link"
	to := mail.NewEmail(creds.Username, creds.Email)
	plainTextContent := msg + verifylink
	htmlContent := msg + "<a href=" + verifylink + ">Verificationlink</a>"
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(sendgridKey)
	response, err := client.Send(message)
	if err != nil {
		fmt.Println(err)
		return false
	}
	fmt.Println(response.StatusCode)
	return true
}

// signup for bidder with 2FA sends verification link in mail
func signupmiddleware(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-type", "application/json")

	var creds SignupCreds
	var resp ResponseData
	var user User

	json.NewDecoder(r.Body).Decode(&creds)

	if len(creds.Username) < 1 || len(creds.Password) < 8 || !strings.Contains(creds.Email, "@") {
		w.WriteHeader(http.StatusBadRequest)
		resp.Message = "Invalid Input fields username > 1, password > 8 chars and email should be valid"
		json.NewEncoder(w).Encode(&resp)
		return
	} else if err = db.Where("username = ?", creds.Username).First(&user).Error; err == nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Message = "Username Already Found"
		json.NewEncoder(w).Encode(&resp)
		return
	} else if err = db.Where("email = ?", creds.Email).First(&user).Error; err == nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Message = "Email Already Found"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	user.Email = creds.Email
	user.Username = creds.Username
	user.Password, err = custjwt.HashPassword(creds.Password)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal error"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	token, err := custjwt.GenerateJWT(w, creds.Username)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal error"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	found := false
	for i, temp := range unverified {
		if temp.Username == user.Username {
			found = true
			unverified[i].Email = user.Email
			unverified[i].Password = user.Password
			unverified[i].Username = user.Username
			break
		}
	}

	if !found {
		unverified = append(unverified, user)
	}

	verifylink := "http://localhost:8080/user/signupverification/" + token
	//verifylink := "https://ctf3030.herokuapp.com/user/signupverification/" + token

	ok := sendmail(verifylink, user, "Click this link to verify your account")

	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal error"
		json.NewEncoder(w).Encode(&resp)
		return
	}
	json.NewEncoder(w).Encode(&ResponseData{Message: "Verification link has been sent to your email"})

}

// signup shortcut for bidder for testing puposes. sends verification link in body
func signupshortcut(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-type", "application/json")

	var creds SignupCreds
	var resp ResponseData
	var user User

	json.NewDecoder(r.Body).Decode(&creds)

	if len(creds.Username) < 1 || len(creds.Password) < 8 || !strings.Contains(creds.Email, "@") {
		w.WriteHeader(http.StatusBadRequest)
		resp.Message = "Invalid Input fields username > 1, password > 8 chars and email should be valid"
		json.NewEncoder(w).Encode(&resp)
		return
	} else if err = db.Where("username = ?", creds.Username).First(&user).Error; err == nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Message = "Username Already Found"
		json.NewEncoder(w).Encode(&resp)
		return
	} else if err = db.Where("email = ?", creds.Email).First(&user).Error; err == nil {
		w.WriteHeader(http.StatusBadRequest)
		resp.Message = "Email Already Found"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	user.Email = creds.Email
	user.Username = creds.Username
	user.Password, err = custjwt.HashPassword(creds.Password)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal error"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	token, err := custjwt.GenerateJWT(w, creds.Username)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal Error"
		return
	}

	found := false
	for _, temp := range unverified {
		if temp.Username == user.Username {
			found = true
			break
		}
	}

	if !found {
		unverified = append(unverified, user)
	}

	verifylink := "http://localhost:8080/user/signupverification/" + token
	//verifylink := "https://ctf3030.herokuapp.com/user/signupverification/" + token

	json.NewEncoder(w).Encode(&ResponseData{Token: verifylink, Message: "Go to this link to verify your account"})

}

// Signup that verifies mail and adds user to table
func signup(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var user User
	var i int
	var resp ResponseData

	urldata := mux.Vars(r)

	token := urldata["token"]

	username, ok := custjwt.AccountVerification(token)

	if !ok {
		w.WriteHeader(http.StatusUnauthorized)
		resp.Message = "Cannot verify link try again"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	found := false
	for i, user = range unverified {
		if username == user.Username {
			found = true
			break
		}
	}

	if !found {
		w.WriteHeader(http.StatusUnauthorized)
		resp.Message = "Cannot verify link try again"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	if db.Table("users").Create(&user).Error != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Message = "Internal error try again"
		json.NewEncoder(w).Encode(&resp)
		return
	}

	unverified = append(unverified[:i], unverified[i+1:]...)

	resp.Token = token
	resp.Message = "Registration Successfull"
	json.NewEncoder(w).Encode(&resp)
}
