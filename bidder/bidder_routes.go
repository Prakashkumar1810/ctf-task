package bidder

import (
	"ctf-task/control"
	"ctf-task/custjwt"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

//Todo : Has to verify whether the token belongs to the User not Admin by checking the DB

//InitHandlers for user routes
func InitHandlers(router *mux.Router) {
	router.HandleFunc("/user/signup", signupmiddleware).Methods("POST")
	router.HandleFunc("/user/googlesignin", googlemiddleware).Methods("GET")
	router.HandleFunc("/user/googleverification", oauthGoogleCallback).Methods("GET")
	router.HandleFunc("/user/signupshortcut", signupshortcut).Methods("POST")
	router.HandleFunc("/user/signupverification/{token}", signup).Methods("GET")
	router.HandleFunc("/user/login", login).Methods("POST")
	router.HandleFunc("/user/bid", custjwt.VerifyJWT(makeBid)).Methods("POST")
	router.HandleFunc("/user/boughtitems", custjwt.VerifyJWT(boughtItems)).Methods("GET")
	router.HandleFunc("/user/forgotpassword", forgotpassword).Methods("POST")
	router.HandleFunc("/user/fotgotverification/{token}", forgotmiddleware).Methods("GET")
	router.HandleFunc("/user/fotgotverification/resetpassword", resetpassword).Methods("POST")
}

//Make Bid for a product
func makeBid(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	username := r.Context().Value(custjwt.ContextKey("username")).(string)

	var bid BidData

	json.NewDecoder(r.Body).Decode(&bid)

	msg, ok := control.MakeBid(username, bid.ProductID, bid.Amount)

	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(&ResponseData{Message: msg})
		return
	}

	json.NewEncoder(w).Encode(&ResponseData{Message: msg})

}

//func to show all the bought items by the user
func boughtItems(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	username := r.Context().Value(custjwt.ContextKey("username")).(string)

	var bids []control.CompletedBid

	if err := db.Where("buyer = ?", username).Find(&bids).Error; err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(&ResponseData{Message: "Internal error"})
		return
	}

	json.NewEncoder(w).Encode(&bids)

}
