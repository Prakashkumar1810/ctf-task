package bidder

//LoginCreds : stores login Credentials from request
type LoginCreds struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

//SignupCreds : stores signup Credentials from request
type SignupCreds struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

// ResponseData : struct for sending response message and JWT
type ResponseData struct {
	Token   string `json:"token,omitempty"`
	Message string `json:"message"`
}

// GoogleCreds which stores google ids.
type GoogleCreds struct {
	Cid     string `json:"cid"`
	Csecret string `json:"csecret"`
}

//GoogleUserInfo :
type GoogleUserInfo struct {
	Email    string `json:"email"`
	Verified bool   `json:"verified_email"`
}

//BidData to get the productid and amount
type BidData struct {
	ProductID int     `json:"product_id"`
	Amount    float64 `json:"amount"`
}
